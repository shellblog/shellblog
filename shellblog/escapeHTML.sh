#!/bin/sh
#
#    This file is part of Shellblog
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>
#    
#    Shellblog is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    Shellblog is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with Shellblog. If not, see <http://www.gnu.org/licenses/>.
#


escapedString=$(echo "$1" | sed -e "s/&/\&amp;/g" -e "s/</\&lt;/g" -e "s/>/\&gt;/g")

echo "$escapedString"
