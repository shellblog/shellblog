#!/bin/sh
#
# This file is part of Shellblog, Licensed under the GNU General Public License v3
# Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>    
# See <http://www.gnu.org/licenses/> for the license terms and conditions.


#constants:
SHELLBLOGVERSION="0.0.1"
SHELLBLOG_ROOT="../shellblog"
POSTDIR="./posts"
CONFIGDIR="./config"

#first of all: send HTTP header:
echo "Content-type: text/html"
echo ""

cd "$SHELLBLOG_ROOT"

#configuration variables:
sed_escaped_postdir=$(echo "$POSTDIR" | sed -e "s/\//\\\\\//g")
blogname=$(cat "$CONFIGDIR/blogname")
style=$(cat "$CONFIGDIR/blogstyle.css")



sendNewestPostTitles()
{
  echo '<div class="NewestEntries">'
  
  echo '</div>'
}

sendPageHeader()
{
  echo '<?xml version="1.0" encoding="utf-8" ?>'
  echo '<html xmlns="http://www.w3.org/1999/xhtml"><head>'
  echo "<title>$blogname - shellblog</title>"
  echo '<style type="text/css">'
  echo "$style"
  echo '</style>'
  echo "</head>"
  echo "<body>"
  echo "<h1>$blogname</h1>"
}


#build a xhtml header:
sendMainContent()
{
  echo '<div id="MainContent">'
  
  #sendNewestPosts
  ./plugins/main/0_blog.sh
  
  echo '</div>'
}


sendPageSidePanel()
{
  echo '<div id="Bar">'
  plugins=$(ls -1 ./plugins/side/*.sh)
  for p in $plugins
  do
    $p
  done
  echo '</div>'
}

sendPageFooter()
{
  echo '<footer id="Footer">'
  echo "This blog runs with Shellblog Version $SHELLBLOGVERSION. Shellblog is released under the GNU General Public License v3."
  echo '</footer></body></html>'
}


sendPageHeader
echo '<div id="MainContentWrapper">'
sendMainContent
sendPageSidePanel
echo '</div>'
sendPageFooter