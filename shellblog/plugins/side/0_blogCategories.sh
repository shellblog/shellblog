#!/bin/sh
#
#    This file is part of Shellblog
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>
#    
#    Shellblog is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    Shellblog is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with Shellblog. If not, see <http://www.gnu.org/licenses/>.
#


#constants:
POSTDIR="./posts"
sed_escaped_postdir=$(echo "$POSTDIR" | sed -e "s/\//\\\\\//g")
CONFIGDIR="./config"

echo '<div class="BarTitle">Categories</div>'
echo '<div class="BarContent">'
echo '(not yet implemented!)<br/>'
#categories=$(find "$POSTDIR"/* -type d)
#for c in $categories


for c in $(cat "$CONFIGDIR/categories.csv")
do
  #echo "DEBUG: $c"
  c_id=$(echo "$c" | cut -d ";" -f 1 | sed "s/$sed_escaped_postdir//g" | sed "s/[/]//g")
  c_name=$(echo "$c" | cut -d ";" -f 2 | sed "s/_/ /g")
  echo "<a class=\"LinkList\" href=\"./shellblog.sh?category=$c_id\">"
  "./escapeHTML.sh" "$c_name" 
  echo "</a>"
done
echo '</div>'