<?php
/*
  This is a PHP wrapper for shellblog.
  Meant for webspaces that don't allow Shell CGI scripts
  but allow PHP scripts.
*/
$output = shell_exec("./shellblog-main.sh"); //execute shellblog

$output = explode("\n\n", $output,2); //get the position of the end of the header
$header = explode("\n",$output[0]); //the header as array of lines
$output = $output[1]; //the XHTML code

foreach($header as $h)
{
  header($h); //print each header field
}

echo "$output"; //output XHTML code
?>